const img = document.getElementById('dog')

var interval
var counter = 0

function start() {
    img.src = 'dog' + counter % 3 + '.gif'
    counter++
}

function load() {
    start()
    interval = setInterval(start, 3000)
}