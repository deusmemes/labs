const box = document.getElementById('box')
const images = ['star5.jpg', 'star51.jpg']
var currentImage = 0

var interval

function start() {
    const current = currentImage % images.length
    box.style.backgroundImage = 'url("' + images[current] + '")'
    currentImage++
}

function load() {
    interval = setInterval(start, 1000)
}