var isShow = true
const div = document.getElementById('box')
const button = document.getElementById('showButton')
button.textContent = 'Скрыть'

function toggle() {
    if (isShow) {
        div.style.display = 'none'
        button.textContent = 'Показать'
    } else {
        div.style.display = 'block'
        button.textContent = 'Скрыть'
    }

    isShow = !isShow
}