const box = document.getElementById('box')
const colorsTable = document.getElementById('colors')
const cells = colorsTable.getElementsByTagName('td')
const colors = ['yellow', 'blue', 'red', 'green']


for (var i = 0; i < cells.length; i++) {
    cells[i].style.backgroundColor = colors[i]
    cells[i].onclick = changeColor
}

function changeColor(e) {
    box.style.backgroundColor = e.target.style.backgroundColor
}