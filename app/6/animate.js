const leftBorder = 70
const rightBorder = 400
const topBorder = 50
const bottomBorder = 300
const box = document.getElementById('box')
const text = document.getElementById('text')

const textWidth = text.clientWidth
const textHeight = text.clientHeight

var texX = 0
var texY = 0
var direction

const width = rightBorder - leftBorder
const height = bottomBorder - topBorder

box.style.width = width + "px"
box.style.height = height + "px"

var interval

function moveRight() {
    texX += 5
    if (texX >= (width - textWidth)) {
        direction = 'left'
    }
}

function moveLeft() {
    texX -= 5
    if (texX <= 0) {
        direction = 'right'
    }
}

function moveDown() {
    texY += 5
    if (texY >= (height - textHeight)) {
        direction = 'up'
    }
}

function moveUp() {
    texY -= 5
    if (texY <= 0) {
        direction = 'down'
    }
}

function moveDiagonalDown() {
    texY += 5
    texX += 5
    if (texY >= (height - textHeight) || texX >= (width - textWidth)) {
        direction = 'diagonal-up'
    }
}

function moveDiagonalUp() {
    texY -= 5
    texX -= 5
    if (texY <= 0) {
        direction = 'diagonal-down'
    }
}


function move() {
    switch (direction) {
        case 'left':
            moveLeft()
            break
        case 'right':
            moveRight()
            break
        case 'up':
            moveUp()
            break
        case 'down':
            moveDown()
            break
        case 'diagonal-down':
            moveDiagonalDown()
            break
        case 'diagonal-up':
            moveDiagonalUp()
            break
    }

    updateTextPosition()
}

function updateTextPosition() {
    text.style.left = texX + "px"
    text.style.top = texY + "px"
}

function moveX() {
    stopMove()
    direction = 'right'
    interval = setInterval(move, 40)
}

function moveY() {
    stopMove()
    direction = 'down'
    interval = setInterval(move, 40)
}

function moveDiagonal() {
    stopMove()
    direction = 'diagonal-down'
    interval = setInterval(move, 40)
}

function stopMove() {
    clearInterval(interval)
}

function load() {

}